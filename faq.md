---
tags: [other]
---

# FAQ

How long does it take for the site to update?
: The [GitLab CI](https://docs.gitlab.com/ee/ci/README.html) build itself takes about ~25 seconds to run. It is generally expected for your site to update around that duration, and take not more than a minute.

What environment is used to build and deploy the site?
: From the [`.gitlab-ci.yml` file](https://gitlab.com/thematten/neuron-template/-/blob/master/.gitlab-ci.yml), it can be seen that we use [Docker image](https://hub.docker.com/repository/docker/sridca/neuron) that comes with current Neuron and minimal shell environment. CI runs job named `pages`, which in turn calls `neuron` on checked out (`master`) working directory. It is set up to output built site to `public` folder, which is where CI expects it for deploying. For more info about how GitLab Pages work, see [GitLab documentation](https://docs.gitlab.com/ee/user/project/pages/).

Can I use my own domain name?
: Yes, see section about [Custom domains and SSL/TLS Certificates](https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/).
