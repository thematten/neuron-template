---
tags: [home]
---

# Neuron Template

You are viewing a [neuron-template](https://gitlab.com/thematten/neuron-template) Zettelkasten published by [neuron](https://neuron.zettel.page/). [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) is used to automatically publish this site every time the underlying Git repository is updated.

Get started by reading <README>.

Other pages on this zettelkasten (this demonstrates how to use tag queries):

<z:zettels?tag=other>
