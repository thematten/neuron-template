# How to publish your own [neuron](https://neuron.zettel.page/) site

[neuron](https://neuron.zettel.page/) is a Zettelkasten-based notes manager, and you can use this template repository to get started with [publishing](https://neuron.zettel.page/778816d3.html) your own neuron site that looks like [one of these](https://neuron.zettel.page/2013101.html).

- Go to <https://gitlab.com/projects/new>
- Select "Import project" tab
- Use <https://gitlab.com/thematten/neuron-template> as Git repository URL
- Give your new project a name, say `mynotes`
- Click "Create project"

GitLab will now build the site, which will become available at `https://<yourgitlabusername>.gitlab.io/mynotes/`.

For more information, see [neuron documentation](https://neuron.zettel.page/) as well as the [GitLab Pages guide](https://docs.gitlab.com/ee/user/project/pages/).

## Set your site metadata

- In your new repository, edit the `neuron.dhall` file to set your site configuration (such as title, author, color theme) to suitable values.

## How to edit and add notes

Assuming you have changed the `editUrl` configuration in `neuron.dhall` (see the above section), you can simply click the "edit" icon on the published site to edit any note (see [GitLab Web Editor](https://docs.gitlab.com/ee/user/project/repository/web_editor.html)). On every change, your site should eventually rebuild.

To understand how linking works, read [the neuron guide on Linking](https://neuron.zettel.page/2011504.html).

For other ways to edit your notes (editors, web interface), see the [neuron guide](https://neuron.zettel.page/2011406.html).
