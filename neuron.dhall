{ siteTitle = "Neuron Template"
, author = Some "John Doe"
, siteBaseUrl = Some "https://thematten.gitlab.io/neuron-template"
-- List of themes: https://neuron.zettel.page/2014601.html
, theme = "teal"
, editUrl = Some "https://gitlab.com/thematten/neuron-template/edit/master/"
, mathJaxSupport = False
}
